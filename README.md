# Sistema de Adquisiciones(Front-end)
![](http://redimensiona.mx/trabajos/logos/logo_lania.jpg "Titulo")

### Descripcion

Esta aplicacion es el "Front-end" de la aplicación [Sistema de Adquisiciones(Back-end)](https://gitlab.com/LeninRamirez/sistema_de_adquisiciones).
Esta compuesta de los 3 casos de uso solicitados en la primera sesion de la materia "Implementación de un Producto de Software".

1. Consultar bases y condiciones para ser proveedor
2. Preguntar sobre licitacion
3. Calificar propuesta tecnicas
***

### Tecnologia y estructura

El proyecto esta realizado con el framework vuejs y vuetify.

- src(carpeta)
    - assets(carpeta)
    - components(carpeta)
    - plugins(carpeta)
    - App.vue(archivo)
    - main.js(archivo)
    - routes.js(archivo)

La carpeta **components** contiene archivos *.vue*, los cuales son vistas de la aplicacion.

La carpeta **plugins** contiene el archivo de configuracion del framework vuetify, el cual
importa los componentes a utilizar del framework vuetify.[vease a la carte](https://vuetifyjs.com/en/guides/a-la-carte)

El archivo **App.vue** es el contenedor principal de la aplicacion, en ella se encuentra la estructura(layout) de diseño.

El archivo **main.js** contiene la instancia de Vue y las importaciones de los diferentes paquetes que se utilizan en la aplicacion.

El archivo **routes.js** contiene las rutas de la aplicacion.Estas rutas son estan relacionadas con los 3 casos de uso mencionadas en la descripcion.
***

### Configuracion del proyecto
```
npm install
```

### Compilacion y carga en caliente para ambiente en desarrollo
```
npm run serve
```

### Compilacion y minificacion para ambiente en produccion
```
npm run build
```
