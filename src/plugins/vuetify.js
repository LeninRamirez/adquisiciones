import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VForm,
  VTextField,
  VSelect,
  VDialog,
  VCard,
  VJumbotron,
  VDivider,
  VAlert,
  VDataTable,
  transitions
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VToolbar,
    VForm,
    VTextField,
    VSelect,
    VDialog,
    VCard,
    VJumbotron,
    VDivider,
    VAlert,
    VDataTable,
    transitions
  },
})
