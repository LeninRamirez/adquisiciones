import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Router from 'vue-router' // Agregar libreria vue router
import routes from '@/routes' // Agregar rutas

Vue.use(Router) // Usar vue router en vue

const router = new Router({routes})

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
