import QualifyTechnicalTender from '@/components/QualifyTechnicalTender.vue'
import Information from '@/components/Information.vue'
import AskAboutLicitacion from '@/components/AskAboutLicitacion.vue'

const routes = [
	{ 
		path : '/information' , 
		component : Information, 
		name :'information'
	},
	{ 
		path : '/qualifyTechnicalTender' , 
		component : QualifyTechnicalTender, 
		name :'qualifyTechnicalTender'
	},
	{ 
		path : '/askAboutLicitacion' , 
		component : AskAboutLicitacion, 
		name :'askAboutLicitacion'
	},
]

export default routes